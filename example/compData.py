#!/bin/python3

import os

datas = open("data.txt", 'r')
datas = datas.read().split('\n')

outputs = []
outputs.append(("Average View: ", "view"))
outputs.append(("AverageMesssage: ", "message"))
outputs.append(("Homogeneity: ", 'homogeneity'))
outputs.append(("Data points: ", "data"))
outputs.append(("Redundandy: ", "redundandy"))
outputs.append(("AverageMemorySize: ", "memory"))
outputs.append(("AverageGuests: ", "guests"))
outputs.append(("StandardDeviation: ", "deviation"))


for metric in outputs:
	output = open(metric[1]+".data", 'w')

	test = lambda s : metric[0] in s
	data = filter(test, datas)
	
	for line in data:
		output.write(line[len(metric[0]):] + '\n')

	output.close()



# Run gnuplot
commands = open('commands', 'w')

commands.write('set term png size 1024,1024\n')
for metric in outputs:
	commands.write('set output "{0}.png"\n'.format(metric[1]))
	# commands.write('plot "{0}.data" with linespoints smooth bezier\n'.format(metric[1]))
	commands.write('plot "{0}.data" with linespoints\n'.format(metric[1]))

commands.close()


os.system("gnuplot commands")

