package KFN.bitvectors

import scala.math.sqrt
import scala.collection.immutable.BitSet
import peersim.core.Node
import peersim.core.CommonState
import KFN.AbstractFramework
import peersim.config.Configuration

class BitVectorsFramework(prefix: String) extends AbstractFramework(prefix) {
  
  val PAR_N = "n"
  val PAR_P = "p"
    
  val n = Configuration.getInt(prefix + "." + PAR_N, 100)
  val p = Configuration.getDouble(prefix + "." + PAR_P, 0.1)
  
  
  var profile: BitSet = BitSet()

  def similarity(a: Node, b: Node) = cosine(a, b)
  
  
  // DANGER: we are not using the canonical definition of cosine but a simplification only possible for bit vectors (a.k.a. Ochiai coefficient)
  def cosine(a: Node, b: Node) = {
    val Abits = a.getProtocol(fwID).asInstanceOf[BitVectorsFramework].profile
    val Bbits = b.getProtocol(fwID).asInstanceOf[BitVectorsFramework].profile
    (Abits intersect Bbits).size / sqrt(Abits.size * Bbits.size)
  }
  
  override def clone(): Object = {
//    println("bitvector clone")
    val nc = (super.clone).asInstanceOf[BitVectorsFramework]
    nc.profile = BitSet()
    for (i <- 1 to n if CommonState.r.nextDouble < p) nc.profile += i
    println(nc.profile)
    return nc
  } 
  
}