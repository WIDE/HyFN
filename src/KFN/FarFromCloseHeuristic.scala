package KFN

import peersim.core.Node

class FarFromCloseHeuristic(framework: AbstractFramework) extends FarCandidatesHeuristic(framework) {
  
  def farCandidates(): View = farFromClose()
  
  def farFromClose(): View = {
    val Vclose = framework.KNN.Vclose
    if (!Vclose.isEmpty) {
      fw.randomPeerIn(Vclose).getProtocol(fw.fwID).asInstanceOf[AbstractFramework].KFN.Vfar
    } else {
      Set[Node]()
    }
  }
  
  
  def select_peer(): Node = {
    fw.randomPeerIn(fw.KNN.Vclose)
  }
  
  def buffer(): View = fw.KFN.Vfar
  

}

