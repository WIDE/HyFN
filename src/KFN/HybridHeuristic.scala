package KFN

import peersim.core.CommonState
import peersim.core.Node

class HybridHeuristic(framework: AbstractFramework) extends FarCandidatesHeuristic(framework) {
  
  val FFC = new FarFromCloseHeuristic(framework)
  val CFF = new CloseFromFarHeuristic(framework)
  var current: String = ""
    
  def farCandidates(): View = {
    if (CommonState.r.nextDouble() < fw.beta) {
      CFF.closeFromFar()  
    }
    else {
      FFC.farFromClose()
    }
  } 

  
  def select_peer(): Node = {
    if (CommonState.r.nextDouble() < fw.beta) {
      current = "CFF"
      CFF.select_peer()
    }
    else {
      current = "FFC"
      FFC.select_peer()
    }
  }
  
  def buffer(): View = {
    if (current == "CFF") CFF.buffer()
    else FFC.buffer()
  }
  

}
