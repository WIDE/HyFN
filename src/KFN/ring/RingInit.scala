package KFN.ring

import peersim.config.Configuration
import peersim.core.Node
import peersim.core.Network
import peersim.core.CommonState
import peersim.core.Control

class RingInit(prefix: String) extends Control {
  
  val PAR_RAND = "random"
  val PAR_FW : String = "frameworkID"

    
  val random = Configuration.getBoolean(prefix + "." + PAR_RAND, true)
  val fwID : Int    = Configuration.getInt(prefix + "." + PAR_FW, 0)

  def execute(): Boolean = {

    val s = Network.size()
    if (random) {
      for (i <- 0 until s) {
        val n = Network.get(i)
        val p = CommonState.r.nextDouble()
//        println(i, p)
        n.getProtocol(fwID).asInstanceOf[RingFramework].position = p
      }
    } else {
      for (i <- 0 until s) {
        val n = Network.get(i)
        val p = i * 1.0/s
        n.getProtocol(fwID).asInstanceOf[RingFramework].position = p
//        println(i, p)
      }
    }
    
    for (i <- 0 until s) {
      val fw = Network.get(i).getProtocol(fwID).asInstanceOf[RingFramework]
      for (j <- 1 to fw.k) {
        var m = i
        while (m == i) {
          m = CommonState.r.nextInt(s)
        } 
        val p = Network.get(m)
        fw.KNN.Vclose += p
        fw.KFN.Vfar += p
      }
    }
   
    false
  }
}