package KFN.ring

import scala.math.{min, abs}
import peersim.core.Node
import peersim.core.CommonState
import KFN.AbstractFramework

class RingFramework(prefix: String) extends AbstractFramework(prefix) {


  var position: Double = -1

  override def clone(): Object = {
//    println("ring clone")
    val nc = (super.clone).asInstanceOf[RingFramework]
//    nc.position = CommonState.r.nextDouble()
    return nc
  }
  
  
  def dist(a: Double, b: Double) : Double = {
    min(abs(a-b), 1 - abs(a-b))
  }

  def pos(n: Node) = n.getProtocol(fwID).asInstanceOf[RingFramework].position

  
  def similarity(a: Node, b: Node): Double = -dist(pos(a), pos(b))

  
  
  
  
}