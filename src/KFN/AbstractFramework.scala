package KFN

import scala.util.Random
import scala.math.{min, abs}
import peersim.cdsim.CDProtocol
import peersim.core.Node
import peersim.core.Network
import peersim.core.CommonState
import peersim.config.Configuration

abstract class AbstractFramework(prefix: String) extends CDProtocol {
  
  type View = Set[Node]
  
  
  val PAR_FW : String = "frameworkID"
  val PAR_A  : String = "alpha"
  val PAR_B  : String = "beta"
  val PAR_K  : String = "k"
  val PAR_R  : String = "r"
  val PAR_HEURISTIC : String = "heuristic"
      
  val fwID : Int    = Configuration.getInt(prefix + "." + PAR_FW, 0)
  val alpha: Double = Configuration.getDouble(prefix + "." + PAR_A, 0.5)
  val beta : Double = Configuration.getDouble(prefix + "." + PAR_B, 0.5)
  val k    : Int    = Configuration.getInt(prefix + "." + PAR_K, 10)
  val r    : Int    = Configuration.getInt(prefix + "." + PAR_R, 5)
  val heuristicKind = Configuration.getString(prefix + "." + PAR_HEURISTIC, "hybrid")
  
   
  var heuristic: FarCandidatesHeuristic = heuristicKind match {
    case "hybrid" => new HybridHeuristic(this)
    case "FfF"    => new FarFromFarHeuristic(this)
    case "FfC"    => new FarFromCloseHeuristic(this)
    case "CfF"    => new CloseFromFarHeuristic(this)
  }

    
  
  
  var KNN = new KNNOverlay(this)
  var KFN = new KFNOverlay(this, heuristic)

  override def clone(): Object = {
//    println("framework clone")
    val nc = (super.clone).asInstanceOf[AbstractFramework]
    nc.heuristic = heuristicKind match {
	    case "hybrid" => new HybridHeuristic(nc)
	    case "FfF"    => new FarFromFarHeuristic(nc)
	    case "FfC"    => new FarFromCloseHeuristic(nc)
	    case "CfF"    => new CloseFromFarHeuristic(nc)
	    } 
    nc.KNN = new KNNOverlay(nc)
    nc.KFN = new KFNOverlay(nc, nc.heuristic)
    return nc
  }
  
  def nextCycle(node: Node, protocolId: Int) {
    if (CommonState.r.nextDouble < alpha) {
//      println("close")
      KNN.updateCloseView(node)
    } else {
      KFN.updateFarView(node)
//      println("far")
    }
  }

  
  
  

  def randomPeerIn(v: View) = {
    v.toIndexedSeq(CommonState.r.nextInt(v.size)) 
  }
  
  def randomView(r: Int) : View = {
    val temp = for (i <- 1 to r) yield Network.get(CommonState.r.nextInt(Network.size()))
    Set() ++ temp
  }


  // Returns a Double higher if a and b are more similar
  // i.e. sim(a,b) > sim(a,c) <==> a closest neighbor is b, not c
  def similarity(a: Node, b: Node): Double
  
  
}