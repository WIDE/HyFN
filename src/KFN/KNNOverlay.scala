package KFN

import peersim.core.Node
import peersim.cdsim.CDProtocol

class KNNOverlay(framework: AbstractFramework) {

  type View = Set[Node]

  var Vclose: View = Set[Node]()

  val fw = framework

  def OLDupdateCloseView(n: Node): View = {
    val localNode = n
    //    println("Update Close")
    val randCands = fw.randomView(fw.r)
    val gossipCands = closeCandidates(n)
    val candidates = Vclose union randCands union gossipCands
    //    def f(p: Node, q: Node) = {dist(position,pos(p)) < dist(position, pos(q))}
    //    candidates.toSeq.sortWith(f).take(k)
    //    def f(n: Node) = fw.dist(fw.position, fw.pos(n))
    def g(n: Node) = -fw.similarity(localNode, n)
    val res = candidates.toSeq.sortBy(g)
    //    print(localNode.getID() + ": ")
    //    for (n <- res) print(n.getID() + ", ")
    //    println()
    res.take(fw.k).toSet
  }

  def closeCandidates(localNode: Node): View = {
    if (!Vclose.isEmpty) {
      val q = fw.randomPeerIn(Vclose)
      // PUSH-PULL Start
      // PUSH
//      val Vprime = Vclose + localNode
      
      // PULL

      // END PUSH-PULL
      q.getProtocol(fw.fwID).asInstanceOf[AbstractFramework].KNN.Vclose
    } else {
      Vclose.empty
    }
  }
  
  
  def updateCloseView(p: Node) {
    val q = select_peer()
    send_buffer_to(q)  // PUSH
    val gossip_candidates = get_buffer_from(q) // PULL
    val candidates = (Vclose union gossip_candidates union fw.randomView(fw.r)) - p
    def h(n: Node) = -fw.similarity(p, n)
    Vclose = candidates.toSeq.sortBy(h).take(fw.k).toSet
   
  }
  
  def select_peer(): Node = {
    fw.randomPeerIn(Vclose)
  }

  def send_buffer_to(q: Node) {
    // PUSH view from p to q
    val q_fw = q.getProtocol(fw.fwID).asInstanceOf[AbstractFramework]
//    val q_randoms = q_fw.randomView(q_fw.r)
    val q_candidates = (q_fw.KNN.Vclose union Vclose) - q //union q_randoms 
    def g(n: Node) = -q_fw.similarity(q, n)
    q_fw.KNN.Vclose = q_candidates.toSeq.sortBy(g).take(q_fw.k).toSet
  }
  
  def get_buffer_from(q: Node): View = {
    // PULL view from q to p
    val q_fw = q.getProtocol(fw.fwID).asInstanceOf[AbstractFramework]
    q_fw.KNN.Vclose
  }
  
}
