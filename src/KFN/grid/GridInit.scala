package KFN.grid

import peersim.core.Control
import peersim.core.Network
import peersim.core.CommonState
import peersim.config.Configuration

class GridInit(prefix: String) extends Control {

  val PAR_FW: String = "frameworkID"
  val fwID: Int = Configuration.getInt(prefix + "." + PAR_FW, 0)

  val PAR_P: String = "p"
  val PAR_Q: String = "q"
  val p: Int = Configuration.getInt(prefix + "." + PAR_P, 10)
  val q: Int = Configuration.getInt(prefix + "." + PAR_Q, 10)
  
  def execute(): Boolean = {

    val s = Network.size()

    for (i <- 0 until s) {
      val n = Network.get(i)
      val a = i / q
      val b = i % q
      val x = b.toDouble/q
      val y = a.toDouble/p
      val pos = (x,y)
      n.getProtocol(fwID).asInstanceOf[GridFramework].position = pos
//      println(i, pos)
    }

    for (i <- 0 until s) {
      val fw = Network.get(i).getProtocol(fwID).asInstanceOf[GridFramework]
      for (j <- 1 to fw.k) {
        var m = i
        while (m == i) {
          m = CommonState.r.nextInt(s)
        }
        val p = Network.get(m)
        fw.KNN.Vclose += p
        fw.KFN.Vfar += p
      }
    }

    false
  }

}