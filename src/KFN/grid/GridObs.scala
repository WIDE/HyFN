package KFN.grid

import peersim.core.Control
import peersim.config.Configuration
import peersim.cdsim.CDState
import peersim.core.Network
import KFN.AbstractFramework
import java.util.Calendar
import java.io.PrintWriter
import java.io.File
import scala.collection.immutable.SortedSet

class GridObs(prefix: String) extends Control {

  val PAR_FW: String = "fwID"
  val PAR_CYCLES: String = "cycles"
  val PAR_LINKFRAC: String = "link_fraction"
  val PAR_NODEFRAC: String = "node_fraction"

  val fwID: Int = Configuration.getInt(prefix + "." + PAR_FW, 0)
  val cycles: Int = Configuration.getInt(prefix + "." + PAR_CYCLES, 100)
  val link_fraction: Double = Configuration.getDouble(prefix + "." + PAR_LINKFRAC, 0.8)
  val node_fraction: Double = Configuration.getDouble(prefix + "." + PAR_NODEFRAC, 0.8)

  var logs: Map[String, IndexedSeq[Any]] = Map()

  var closeTruthTable: Map[Int, Set[Int]] = Map()
  var farTruthTable: Map[Int, Set[Int]] = Map()

  def execute(): Boolean = {
    if (CDState.getCycle() == 0) {
      fillTruthTable()
      logs = logs + ("MissingCloseLinks" -> IndexedSeq[Int]()) +
        ("MissingFarLinks" -> IndexedSeq[Int]()) +
        ("AvgFarSim" -> IndexedSeq[Double]()) +
        ("AvgCloseSim" -> IndexedSeq[Double]()) +
        ("FarCvgdNodes" -> IndexedSeq[Double]()) +
        ("CloseCvgdNodes" -> IndexedSeq[Int]())
    }

    val s = Network.size()

    var AvgFarSim: Double = 0
    var AvgCloseSim: Double = 0
    var missingCloseLinks: Int = 0
    var missingFarLinks: Int = 0
    var FarCvgdNodes: Double = 0
    var CloseCvgdNodes: Double = 0

    for (i <- 0 until s) {
      val n = Network.get(i)
      var closeCount = 0
      var farCount = 0

      //      print("i = " + i + ": ")
      val fw = n.getProtocol(fwID).asInstanceOf[AbstractFramework]
      import fw.{ similarity => sim }

      for (v <- closeTruthTable(i)) {
        val q = Network.get(v)
        if (!(fw.KNN.Vclose contains q)) {
          missingCloseLinks += 1
          closeCount += 1
        }
      }
      if (closeCount < 0.2 * fw.k) CloseCvgdNodes += 1
      for (v <- farTruthTable(i)) {
        val q = Network.get(v)
        if (!(fw.KFN.Vfar contains q)) {
          missingFarLinks += 1
          farCount += 1
          //          print(v + ", ")

        }
      }
      if (farCount < 0.2 * fw.k) FarCvgdNodes += 1
      //      println()

      var LocalFarSim: Double = 0
      for (p <- fw.KFN.Vfar) {
        //        LocalFarSim += dist(pos(n), pos(p))
        LocalFarSim += sim(n, p)
      }
      if (!fw.KFN.Vfar.isEmpty) AvgFarSim += LocalFarSim / fw.KFN.Vfar.size

      var LocalCloseSim: Double = 0
      for (p <- fw.KNN.Vclose) {
        //        LocalCloseSim += dist(pos(n), pos(p))
        LocalCloseSim += sim(n, p)
      }
      if (!fw.KNN.Vclose.isEmpty) AvgCloseSim += LocalCloseSim / fw.KNN.Vclose.size
    }

    if (s > 0) {
      AvgFarSim /= s
      AvgCloseSim /= s
      CloseCvgdNodes /= s
      FarCvgdNodes /= s
    }
    logs = logs + ("AvgCloseSim" -> (logs("AvgCloseSim") :+ AvgCloseSim)) +
      ("AvgFarSim" -> (logs("AvgFarSim") :+ AvgFarSim)) +
      ("MissingCloseLinks" -> (logs("MissingCloseLinks") :+ missingCloseLinks)) +
      ("MissingFarLinks" -> (logs("MissingFarLinks") :+ missingFarLinks)) +
      ("CloseCvgdNodes" -> (logs("CloseCvgdNodes") :+ CloseCvgdNodes)) +
      ("FarCvgdNodes" -> (logs("FarCvgdNodes") :+ FarCvgdNodes))

    if (CDState.getCycle() == cycles - 1) {
      logs2CSV()
    }

    //    graphstring2file(graph2string())
    false
  }

  def fillTruthTable() {

    println("Filling truth table.")
    var s = Network.size()
    val fw = Network.get(0).getProtocol(fwID).asInstanceOf[GridFramework]
    import fw.{ dist, pos }
    for (i <- 0 until s) {
      val localNode = Network.get(i).getProtocol(fwID).asInstanceOf[GridFramework]
      val localPos = localNode.position
      val closeOrdering = Ordering.by[(Int,Double), (Double,Int)]({case (n,d) => (d, n)})
      val farOrdering = Ordering.by[(Int,Double), (Double,Int)]({case (n,d) => (-d, n)})
      var close = SortedSet[(Int, Double)]()(closeOrdering)
      var far = SortedSet[(Int, Double)]()(farOrdering)
      for (j <- 0 until s if j != i) {
        val distantNode = Network.get(j).getProtocol(fwID).asInstanceOf[GridFramework]
        val distantPos = distantNode.position
        val t = close.size
        close = close + ((j, dist(localPos, distantPos)))
        far = far + ((j, dist(localPos, distantPos)))
      }
      closeTruthTable = closeTruthTable + (i -> close.take(fw.k).map(_._1))
      farTruthTable = farTruthTable + (i -> far.take(fw.k).map(_._1))
      
    }
  }

  def logs2CSV() {
    var s = "Round\t"
    val keys = logs.keys.toSeq
    for (k <- keys) s = s + k + "\t"
    s = s + "\n"
    val R = logs(keys(1)).size
    for (r <- 0 until R) {
      s = s + r + "\t"
      for (k <- keys) {
        s = s + logs(k)(r) + "\t"
      }
      s = s + "\n"

    }
    println(s)

  }

}