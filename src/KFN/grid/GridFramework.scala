package KFN.grid

import math.{abs, sqrt}
import KFN.AbstractFramework
import peersim.core.Node

class GridFramework(prefix: String) extends AbstractFramework(prefix) {
  
  type Position = (Double, Double)
  
  var position: (Double, Double) = (-1, -1)
  
  override def clone(): Object = {
//    println("grid clone")
    val nc = (super.clone).asInstanceOf[GridFramework]
    return nc
  }

  def dist(a: Position, b: Position) : Double = {
	sqrt((a._1 - b._1)*(a._1 - b._1) + (a._2 - b._2)*(a._2 - b._2))
  }

  def pos(n: Node) = n.getProtocol(fwID).asInstanceOf[GridFramework].position

  
  def similarity(a: Node, b: Node): Double = -dist(pos(a), pos(b))

  
  
  
}