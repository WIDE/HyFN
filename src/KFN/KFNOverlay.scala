package KFN

import peersim.cdsim.CDProtocol
import peersim.core.Node

class KFNOverlay(framework: AbstractFramework, heuristic: FarCandidatesHeuristic) {
  
  type View = Set[Node]

  val fw = framework
  val heur = heuristic
  

  var Vfar: View = Set[Node]()
  
  
  def OLDupdateFarView(n: Node) : View = {
    val localNode = n
    val randCands = fw.randomView(fw.r)
    val gossipCands = heuristic.farCandidates()
    val candidates = Vfar union randCands union gossipCands
//    def f(p: Node, q: Node) = {fw.dist(fw.position,fw.pos(p)) > fw.dist(fw.position, fw.pos(q))}
    def g(p: Node, q: Node) = fw.similarity(localNode, p) < fw.similarity(localNode, q)
    def h(p: Node) = fw.similarity(localNode, p)
//    (candidates.toSeq sortWith g take fw.k).toSet
    (candidates.toSeq sortBy h take fw.k).toSet
  }
  
  
  
  def updateFarView(p: Node) {
    val q = heuristic.select_peer()
    send_buffer_to(q)  // PUSH
    val gossip_candidates = get_buffer_from(q) // PULL
    val candidates = (Vfar union gossip_candidates union fw.randomView(fw.r)) - p
    def h(n: Node) = fw.similarity(p, n)
    fw.KFN.Vfar = (candidates.toSeq sortBy h take fw.k).toSet
   
  }
  

  def send_buffer_to(q: Node) {
    // PUSH view from p to q
    val q_fw = q.getProtocol(fw.fwID).asInstanceOf[AbstractFramework]
//    val q_randoms = q_fw.randomView(q_fw.r)
    val q_candidates = (q_fw.KFN.Vfar union heuristic.buffer()) - q//union q_randoms 
    def h(n: Node) = q_fw.similarity(q, n)
    q_fw.KFN.Vfar = (q_candidates.toSeq sortBy h take fw.k).toSet
  }
  
  def get_buffer_from(q: Node): View = {
    // PULL view from q to p
    val q_fw = q.getProtocol(fw.fwID).asInstanceOf[AbstractFramework]
    q_fw.KFN.heur.buffer()
  }

}