package KFN

import peersim.core.Node

class CloseFromFarHeuristic(framework: AbstractFramework) extends FarCandidatesHeuristic(framework) {
  
  def farCandidates(): View = closeFromFar()
  
  def closeFromFar(): View = {
    val Vfar = fw.KFN.Vfar
    if (!Vfar.isEmpty) { 
      fw.randomPeerIn(Vfar).getProtocol(fw.fwID).asInstanceOf[AbstractFramework].KNN.Vclose
    } else {
      Set[Node]()
    }
  }
  
  
  def select_peer(): Node = {
    fw.randomPeerIn(fw.KFN.Vfar)
  }
  
  def buffer(): View = fw.KNN.Vclose
  
}

