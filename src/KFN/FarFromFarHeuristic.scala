package KFN

import peersim.core.Node

class FarFromFarHeuristic(framework: AbstractFramework) extends FarCandidatesHeuristic(framework) {
  
  def farCandidates(): View = farFromFar()
  
  def farFromFar(): View = {
    val Vfar = fw.KFN.Vfar
    if (!Vfar.isEmpty) {
      fw.randomPeerIn(Vfar).getProtocol(fw.fwID).asInstanceOf[AbstractFramework].KFN.Vfar
    } else {
      Set[Node]()
    }
  }
  
  
  def select_peer(): Node = {
    fw.randomPeerIn(fw.KFN.Vfar)
  }
  
  def buffer(): View = fw.KFN.Vfar
  

}
