package KFN

import peersim.core.Node

abstract class FarCandidatesHeuristic(framework: AbstractFramework) {
  
  type View = Set[Node]

  def fw = framework
  
  def farCandidates(): View
  
  def select_peer(): Node
  
  def buffer(): View
  
}