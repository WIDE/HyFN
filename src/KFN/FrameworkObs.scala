package KFN

import peersim.core.Network
import peersim.core.Control
import peersim.config.Configuration
import peersim.cdsim.CDState
import peersim.core.CommonState
import java.util.Calendar
import java.io.PrintWriter
import java.io.File
import KFN.ring.RingFramework

class FrameworkObs(prefix: String) extends Control {

  val PAR_FW: String = "fwID"
  val PAR_MODE: String = "mode"

  val fwID: Int = Configuration.getInt(prefix + "." + PAR_FW, 0)
  val mode: String = Configuration.getString(prefix + "." + PAR_MODE, "dev")

  var closeTruthTable: Map[Int, Set[Int]] = Map()
  var farTruthTable: Map[Int, Set[Int]] = Map()

  var logs: Map[String, IndexedSeq[Any]] = Map()

  def execute(): Boolean = {

    if (mode == "dev") {
      if (CDState.getCycle() == 0) {
        fillTruthTable()
        for (i <- 0 until Network.get(1).protocolSize())
          println(i, Network.get(1).getProtocol(i))
      }

      val s = Network.size()

      var AvgFarSim: Double = 0
      var AvgCloseSim: Double = 0
      var missingCloseLinks: Int = 0 
      var missingFarLinks: Int = 0

      for (i <- 0 until s) {
        val n = Network.get(i)

        val fw = n.getProtocol(fwID).asInstanceOf[AbstractFramework]
        import fw.{ similarity => sim }

        for (v <- closeTruthTable(i)) {
          val q = Network.get(v)
          if (!(fw.KNN.Vclose contains q)) missingCloseLinks += 1
        }
        for (v <- farTruthTable(i)) {
          val q = Network.get(v)
          if (!(fw.KFN.Vfar contains q)) missingFarLinks += 1
        }

        var LocalFarSim: Double = 0
        for (p <- fw.KFN.Vfar) {
          //        LocalFarSim += dist(pos(n), pos(p))
          LocalFarSim += sim(n, p)
        }
        if (!fw.KFN.Vfar.isEmpty) AvgFarSim += LocalFarSim / fw.KFN.Vfar.size

        var LocalCloseSim: Double = 0
        for (p <- fw.KNN.Vclose) {
          //        LocalCloseSim += dist(pos(n), pos(p))
          LocalCloseSim += sim(n, p)
        }
        if (!fw.KNN.Vclose.isEmpty) AvgCloseSim += LocalCloseSim / fw.KNN.Vclose.size
      }

      if (s > 0) {
        AvgFarSim /= s
        AvgCloseSim /= s
      }

      println("\tMissingCloseLinks = " + missingCloseLinks)
      println("\tMissingFarLinks = " + missingFarLinks)
      println("\tAvgFarSim = " + AvgFarSim)
      println("\tAvgCloseSim = " + AvgCloseSim)
    } else if (mode == "batch") {
      if (CDState.getCycle() == 0) {
        fillTruthTable()
        logs = logs + ("MissingCloseLinks" -> IndexedSeq[Int]()) + ("MissingFarLinks" -> IndexedSeq[Int]()) + ("AvgFarSim" -> IndexedSeq[Double]()) + ("AvgCloseSim" -> IndexedSeq[Double]())
      }

      val s = Network.size()

      var AvgFarSim: Double = 0
      var AvgCloseSim: Double = 0
      var missingCloseLinks: Int = 0
      var missingFarLinks: Int = 0

      for (i <- 0 until s) {
        val n = Network.get(i)

        val fw = n.getProtocol(fwID).asInstanceOf[AbstractFramework]
        import fw.{ similarity => sim }

        var count = 0
        for (v <- closeTruthTable(i)) {
          val q = Network.get(v)
          if (!(fw.KNN.Vclose contains q)) { missingCloseLinks += 1; count += 1 }
        }
//        if (count > 0) println(i)
        for (v <- farTruthTable(i)) {
          val q = Network.get(v)
          if (!(fw.KFN.Vfar contains q)) missingFarLinks += 1
        }

        var LocalFarSim: Double = 0
        for (p <- fw.KFN.Vfar) {
          //        LocalFarSim += dist(pos(n), pos(p))
          LocalFarSim += sim(n, p)
        }
        if (!fw.KFN.Vfar.isEmpty) AvgFarSim += LocalFarSim / fw.KFN.Vfar.size

        var LocalCloseSim: Double = 0
        for (p <- fw.KNN.Vclose) {
          //        LocalCloseSim += dist(pos(n), pos(p))
          LocalCloseSim += sim(n, p)
        }
        if (!fw.KNN.Vclose.isEmpty) AvgCloseSim += LocalCloseSim / fw.KNN.Vclose.size
      }

      if (s > 0) {
        AvgFarSim /= s
        AvgCloseSim /= s
      }
      logs = logs + ("AvgCloseSim" -> (logs("AvgCloseSim") :+ AvgCloseSim)) +
        ("AvgFarSim" -> (logs("AvgFarSim") :+ AvgFarSim)) +
        ("MissingCloseLinks" -> (logs("MissingCloseLinks") :+ missingCloseLinks)) +
        ("MissingFarLinks" -> (logs("MissingFarLinks") :+ missingFarLinks))
      if (CDState.getCycle() == 99) {
        logs2CSV()
      }

    } else println("ERROR")

//    graphstring2file(graph2string())
    false
  }

  def fillTruthTable() {
    println("Filling truth table.")
    var s = Network.size()
    val fw = Network.get(0).getProtocol(fwID).asInstanceOf[RingFramework]
    import fw.{ dist, pos }
    for (i <- 0 until s) {
      var n = Network.get(i)
      var p = pos(n)
      var n1: Int = -1
      var d1 = 2.0
      var n2: Int = -1
      var d2 = 2.0
      var na: Int = -1
      var da = 0.0
      var nb: Int = -1
      var db = 0.0
      for (j <- 0 until s if j != i) {
        var c = Network.get(j)
        var cp = pos(c)
        var d: Double = 3.0
        d = dist(p, cp)
        if (d < d1) {
          n2 = n1
          d2 = d1
          n1 = j
          d1 = d
        } else if (d < d2) {
          n2 = j
          d2 = d
        }
        if (d > da) {
          nb = na
          db = da
          na = j
          da = d
        } else if (d > db) {
          nb = j
          db = d
        }
      }

      closeTruthTable = closeTruthTable + (i -> Set(n1, n2))
      //      println(n1)
      //      println(n2)
      //      println("-------")
      farTruthTable = farTruthTable + (i -> Set(na, nb))

    }
    println(closeTruthTable)
    println(farTruthTable)

  }

  def logs2CSV() {
    var s = "Round\t"
    val keys = logs.keys.toSeq
    for (k <- keys) s = s + k + "\t"
    s = s + "\n"
    val R = logs(keys(1)).size
    for (r <- 0 until R) {
      s = s + r + "\t"
      for (k <- keys) {
        s = s + logs(k)(r) + "\t"
      }
      s = s + "\n"

    }
    println(s)

  }

  def graph2string(): String = {
    var res = "graph G {\n"
    var edges = ""
    var attributes = ""

    //      var missingneighbors: Int = 0

    var s = Network.size()
    for (i <- 0 until s) {
      var current = Network.get(i)
      val fw = current.getProtocol(fwID).asInstanceOf[AbstractFramework]

      var neighbors = fw.KNN.Vclose filter (x => x != null)
      var id = current.getID().toString()
      attributes += id 
      for (n <- neighbors) {
        edges += id + " -- " + n.getID() + ";"
      }
      edges += "\n"
    }
    //      println("we miss neighbors: " + missingneighbors)
    res += edges + attributes + "}\n"
    res
  }

  def graphstring2file(s: String) {
    val today = Calendar.getInstance().getTime().getTime()
    val writer = new PrintWriter(new File(s"/home/sbouget/graphviz/$today.dot"))
    writer.write(s)
    writer.close()
  }

}