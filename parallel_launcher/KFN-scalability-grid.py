#!/usr/bin/env python3
# -*-  python-indent:2; tab-width:2; indent-tabs-mode:t; -*-

# DONE: monotonic behaviour. Does not overwrite existing result files. Just skip experiment.
# DONE: graceful handling of exception (print message, continue further)
# DONE: test with "progressive" length
# DONE: move concatenation of expe to second script
# DONE: factor out multiprocessing to external script


import random
import math

import simulationLauncher as launcher

myTemplate = """
# KFN dummy 

cycles 100

simulation.cycles cycles
simulation.experiments 1


random.seed {seed}

q = {q}
p = {p}
network.size = p*q

protocol.fw KFN.grid.GridFramework
protocol.fw.alpha	{alpha}
protocol.fw.beta	{beta}
protocol.fw.k	{k}
protocol.fw.r	{r}
protocol.fw.heuristic {heuristic}

control.obs KFN.grid.GridObs
control.obs.cycles cycles

init.grid KFN.grid.GridInit
init.grid.p	p
init.grid.q q

"""


random.seed(0) # set seed for reproducibility

# listing all experimental run in one large array
nb_expe = 25

def generate_config_file(args,seed):
	return myTemplate.format(seed=seed, p=args["p"], q=args["q"], alpha=args["alpha"], beta=args["beta"], k=args["k"], r=args["r"], heuristic=args["heuristic"])

alpha = 0.5
beta = 0.5
heuristic = "hybrid"

for size in (100, 200, 400, 800, 1600, 3200, 6400, 10000, 12800):
#for size in (100, 200, 400):
	t = math.log2(size)
	k = round(1.2*t)
	r = round(1.2*t/4)
	u = math.floor(math.sqrt(size))
	p = round(u, -1)
	q = round(size / p)
	print(p,q,p*q)
	continue

	listOfRunParams = [
		({"p":p, "q":q, "alpha":alpha, "beta":beta, "k":k, "r":r, "heuristic":heuristic},
		expe_nb,random.randint(-2**63,2**63-1))
		for expe_nb in range(0,nb_expe)
		]

	launcher.parse_args()
	launcher.set_expe_name_from_file_name(
		"(.*)KFN-(.*).py",__file__,"_S{}_A{}_B{}_K{}_R{}_H{}".format(size,alpha,beta,k,r,heuristic)
		)
#				 		launcher.set_expe_name("S{}_C{}".format(size,nb_comp))
#				 		launcher.set_expe_name("2015-11-02-test222")
#						launcher.set_expe_name("test")
	launcher.set_config_factory(generate_config_file)
	launcher.launch_parallel(listOfRunParams)

