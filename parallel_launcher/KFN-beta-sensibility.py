#!/usr/bin/env python3
# -*-  python-indent:2; tab-width:2; indent-tabs-mode:t; -*-

# DONE: monotonic behaviour. Does not overwrite existing result files. Just skip experiment.
# DONE: graceful handling of exception (print message, continue further)
# DONE: test with "progressive" length
# DONE: move concatenation of expe to second script
# DONE: factor out multiprocessing to external script


import random

import simulationLauncher as launcher

myTemplate = """
# KFN dummy 

cycles 60

simulation.cycles cycles
simulation.experiments 1


random.seed {seed}

network.size {size}

protocol.fw KFN.RingFramework
protocol.fw.alpha	{alpha}
protocol.fw.beta	{beta}
protocol.fw.k	{k}
protocol.fw.r	{r}
protocol.fw.heuristic {heuristic}

control.obs KFN.FrameworkObs
control.obs.mode batch

init.ring KFN.RingInit
init.ring.random false

"""


random.seed(0) # set seed for reproducibility

# listing all experimental run in one large array
nb_expe = 25

def generate_config_file(args,seed):
	return myTemplate.format(seed=seed, size=args["size"], alpha=args["alpha"], beta=args["beta"], k=args["k"], r=args["r"], heuristic=args["heuristic"])

size = 3200
alpha = 0.5
k = 10
r = 0
heuristic = "hybrid"

for beta in (0,0.2,0.4,0.6,0.8,1):

	listOfRunParams = [
		({"size":size, "alpha":alpha, "beta":beta, "k":k, "r":r, "heuristic":heuristic},
		expe_nb,random.randint(-2**63,2**63-1))
		for expe_nb in range(0,nb_expe)
		]

	launcher.parse_args()
	launcher.set_expe_name_from_file_name(
		"(.*)KFN-(.*).py",__file__,"_S{}_A{}_B{}_K{}_R{}_H{}".format(size,alpha,beta,k,r,heuristic)
		)
#				 		launcher.set_expe_name("S{}_C{}".format(size,nb_comp))
#				 		launcher.set_expe_name("2015-11-02-test222")
#						launcher.set_expe_name("test")
	launcher.set_config_factory(generate_config_file)
	launcher.launch_parallel(listOfRunParams)

